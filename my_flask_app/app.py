import sys
import os

from flask import Flask, render_template, request

# Add the parent directory of your_module to the Python path
module_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'your_module'))
if module_path not in sys.path:
    sys.path.append(module_path)

# Now you can import from your_module
from your_module.rule_handler import base_rules, generate_arg, generate_rebuts, deffeat, display_rebuts_grouped_by_conclusion
from your_module.rule_converter import parse_user_input, serialize_rules

# Create a Flask app instance
app = Flask(__name__)

#TODO voire  a corrigée le faite que quand on lance plusieurs fois d'affiler le traitement des regles ne numeros des regles n'est pas réinitialiser

# Define the route for the homepage
@app.route('/', methods=['GET', 'POST'])
def index():
    #TODO handle error to print theme instead of crashing the page 


    user_input = ""  # Initialize user_input variable

    if request.method == 'POST':
        # Get user input from the form
        user_input = request.form['user_input']
        # Here you can handle the user input if needed

        
    # converts the input into rules
    rule_from_string = []
    rule_from_string = parse_user_input(user_input)
    original_rule_from_string = rule_from_string.copy()


    # Execute the arg function to generate new arguments
    new_arguments = []
    new_arguments = generate_arg(rule_from_string)

    # Extract rule strings from new_arguments
    for arg in new_arguments:
        rule_from_string.append(str(arg.top_rule))

    # Generate rebuts for the new arguments
    all_rebuts = []
    all_rebuts = generate_rebuts(new_arguments)

    # Generate defeats
    all_defeats = {}
    all_attackers = []
    all_defeats, all_attackers = deffeat(new_arguments)


    # Display rebuts grouped by conclusion
    rebut_groups = []
    rebut_groups = display_rebuts_grouped_by_conclusion(all_rebuts)

    # Render the index.html template with the results  
    # return render_template('index.html', user_input=user_input, rule_from_string=rule_from_string, rebut_groups=rebut_groups)
    # return render_template('index.html', user_input=original_rule_from_string, rule_strings=rule_from_string, rebut_groups=rebut_groups)
    return render_template('index.html', user_input=original_rule_from_string, rule_strings=rule_from_string, rebut_groups=rebut_groups, all_defeats=all_defeats, all_attackers=all_attackers)

# Run the Flask app
if __name__ == '__main__':
    app.run(debug=True)




#TODO ajout l'affichage des undercut 
#TODO deplayer
#TODO faire le graphe
#TODO afficher les numero des argumentes A12: [r1]