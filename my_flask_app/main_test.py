import sys
import os

from flask import Flask, render_template, request

# Add the parent directory of your_module to the Python path
module_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'your_module'))
if module_path not in sys.path:
    sys.path.append(module_path)

# Now you can import from your_module
from your_module.rule_handler import base_rules, generate_arg, generate_rebuts, deffeat, display_rebuts_grouped_by_conclusion
from your_module.rule_converter import parse_user_input, serialize_rules


user_input = """[r1] -> a 1
[r2] d,b => c 1
[r3] !c -> d 1
[r10] !c,b -> !d 1
[r11] !c,d -> !b 1
[r12] !d -> c 1
[r4] a => !d 0
[r5] => b 1
[r6] => !c 1
[r7] => d 0
[r8] c => e 0
[r9] !c => !r4 0"""  # Initialize user_input variable



# converts the input into rules
rule_from_string = []
rule_from_string = parse_user_input(user_input)
original_rule_from_string = rule_from_string.copy()


# Execute the arg function to generate new arguments
new_arguments = []
new_arguments = generate_arg(rule_from_string)

# Extract rule strings from new_arguments
for arg in new_arguments:
    rule_from_string.append(str(arg.top_rule))

# Generate rebuts for the new arguments
all_rebuts = []
all_rebuts = generate_rebuts(new_arguments)

# Generate defeats
all_defeats = {}
all_attackers = {}

all_defeats, all_attackers = deffeat(new_arguments)


# Display rebuts grouped by conclusion
rebut_groups = []
rebut_groups = display_rebuts_grouped_by_conclusion(all_rebuts)


# for deffeat in all_defeats:
    # print(deffeat)
print("all_deafeat")

# Display defeats
print("Défenses :")
for defender, attackers in all_defeats.items():
    print(f"{defender} défendu par : {all_attackers[0]}")


#TODO il faut 59 rebut
